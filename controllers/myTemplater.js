/* myTemplater.js nml templating */
"use strict";

exports.doTheMagic = function(data, obj) {
    data = '' + data;                   // buffer to string
    let regex = /xpq/g;                 // dummy regex
    let a = Object.keys(obj);           // get keys from 4th param
    a.forEach(function(doo) {           // loop through then
        let regstr = `<42 ${doo} 24>`;  // materialize regex string from key
        regex = RegExp(regstr, 'g');    // create regex from that
        let s = '';
        if (typeof obj[doo] == 'object') {  // if object
            s = `<table>`;
            for (let c of obj[doo]) {       //  loop through obj (array)
                let user = new User(c.name, c.email, c.phone);
                s += user.toString();
            }
            s += `</table>`;
        } else if (typeof obj[doo] == 'string') {
            s = obj[doo];                   // if string, place it
        }
        data = data.replaceAll(regex, s);   // actual replace
    });

    return data;
};

exports.doStatistics = function(data, obj) {
   data = '' + data;                   // buffer to string
    let regex = /xpq/g;                 // dummy regex
    let a = Object.keys(obj);           // get keys from 4th param
    a.forEach(function(doo) {  
								        // loop through then
        let regstr = `<42 ${doo} 24>`;  // materialize regex string from key
        regex = RegExp(regstr, 'g');    // create regex from that
        let s = '';
        if (typeof obj[doo] == 'object') {  // if object
            s = `<table>`;
            s += `<tr><td>Kortes navn</td><td>Beskrivelse af udførelse</td><td>Dato for udførelse</td></tr>`;
            for (let c  of obj[doo]) {   //  loop through obj (array)
				// console.log(obj[doo]);
				// console.log("lenght : " + c.length);
				// for(let i = 0; i <= c.length; i ++){
					s += `<tr><td>`+ c[0].texttitlefront + `</td><td>` + c[0].description + `</td><td>`+ c[0].datedone + `</td></tr>`;
            }
            s += `</table>`;
        } else if (typeof obj[doo] == 'string') {
            s = obj[doo];                   // if string, place it
        }
        data = data.replaceAll(regex, s);   // actual replace
        });

    return data;
};

exports.doCard = function(data, obj) {
    data = '' + data;                   // buffer to string
    let regex = /xpq/g;                 // dummy regex
    let a = Object.keys(obj);           // get keys from 4th param
    a.forEach(function(doo) { 
		          // loop through then
        let regstr = `<42 $a[0] 24>`;  // materialize regex string from key
        regex = RegExp(regstr, 'g');    // create regex from that
        let s;
        if (typeof a[0] == 'object') {  // if object
            s += `<table>`;
            s += `<tr><td>Kortes navn</td><td>Beskrivelse af udførelse</td><td>Dato for udførelse</td></tr>`;
            for (let c  of a[0]) {   //  loop through obj (array)
                s += `<tr><td>${c[0].texttitlefront}</td><td>${c[0].description}</td><td>${c[0].datedone}</td></tr>`;
            }
            s += `</table>`;

			//Kort hvor forsiden har to koloner.
			//Rød
            // s = `<div class="card">`;
			// s = `<div class="front4x2red">`;
            // s = `<p class="texttitlefront paddingTop">` + obj.exercise + `</p>`;
            // s = `<p class="underText">` + obj.exercise + `</p>`;
			// s = `<div class="row">`;
			// s = `<p class="exerciseTitle2">` + obj.exercise + `</p>`;
			// s = `<p class="exerciseText">` + obj.exercise + `</p>`;
			// s += `</div>`;
			// s = `<div class="row">`;
			// s = `<div>`;
			// s = `<p class="exercise2">` + obj.exercise + `</p>`;
			// s = `<p class="exercise2">` + obj.exercise + `</p>`;
			// s = `<p class="exercise2">` + obj.exercise + `</p>`;
			// s = `<p class="exercise2">` + obj.exercise + `</p>`;
			// s += `</div>`;
			// s = `<div>`;
			// s = `<p class="number">` + obj.exercise + `</p>`;
			// s = `<p class="number">` + obj.exercise + `</p>`;
			// s = `<p class="number">` + obj.exercise + `</p>`;
			// s = `<p class="number">` + obj.exercise + `</p>`;
			// s += `</div>`;
			// s += `</div>`;
			// s = `<p class="hashtag paddingBottom">` + obj.exercise + `</p>`;
			// s += `</div>`;
			// s = `<div class="backRed">`;
			// s = `<p class="texttitleback paddingTopBack">` + obj.exercise + `</p>`;
			// s = `<p class="underTextBack">` + obj.exercise + `</p>`;
			// s = `<div class="row center">`;
			// s = `<div class="column timerColumn">`;
			// s = `<p class="timer">` + obj.exercise + `</p>`;
			// s = `<img class="timer" src="image/Timer.png" alt="Timer">`;
			// s += `</div>`;
			// s = `<div class="column qrColumn">`;
			// s = `<p class="qr">` + obj.exercise + `</p>`;
			// s = `<img class="qrIcon" src="image/qr.png" alt="QR kode">`;
			// s += `</div>`;
			// s += `</div>`;
			// s = `<div class="column paddingBottomBack">`;
			// s = `<p class="difficultyLevel">` + obj.exercise + `</p>`;
			// s = `<img class="difficultyLevel1Icon" src="image/difficulty1.png" alt="Difficulty level">`
			// s += `</div>`;
			// s += `</div>`;
			// s += `</div>`;
			// //Grøn
			// s = `<div class="card">`;
			// s = `<div class="front4x2green">`;
            // s = `<p class="texttitlefront paddingTop">` + obj.exercise + `</p>`;
            // s = `<p class="underText">` + obj.exercise + `</p>`;
			// s = `<div class="row">`;
			// s = `<p class="exerciseTitle2">` + obj.exercise + `</p>`;
			// s = `<p class="exerciseText">` + obj.exercise + `</p>`;
			// s += `</div>`;
			// s = `<div class="row">`;
			// s = `<div>`;
			// s = `<p class="exercise2">` + obj.exercise + `</p>`;
			// s = `<p class="exercise2">` + obj.exercise + `</p>`;
			// s = `<p class="exercise2">` + obj.exercise + `</p>`;
			// s = `<p class="exercise2">` + obj.exercise + `</p>`;
			// s += `</div>`;
			// s = `<div>`;
			// s = `<p class="number">` + obj.exercise + `</p>`;
			// s = `<p class="number">` + obj.exercise + `</p>`;
			// s = `<p class="number">` + obj.exercise + `</p>`;
			// s = `<p class="number">` + obj.exercise + `</p>`;
			// s += `</div>`;
			// s += `</div>`;
			// s = `<p class="hashtag paddingBottom">` + obj.exercise + `</p>`;
			// s += `</div>`;
			// s = `<div class="backGreen">`;
			// s = `<p class="texttitleback paddingTopBack">` + obj.exercise + `</p>`;
			// s = `<p class="underTextBack">` + obj.exercise + `</p>`;
			// s = `<div class="row center">`;
			// s = `<div class="column timerColumn">`;
			// s = `<p class="timer">` + obj.exercise + `</p>`;
			// s = `<img class="timer" src="image/Timer.png" alt="Timer">`;
			// s += `</div>`;
			// s = `<div class="column qrColumn">`;
			// s = `<p class="qr">` + obj.exercise + `</p>`;
			// s = `<img class="qrIcon" src="image/qr.png" alt="QR kode">`;
			// s += `</div>`;
			// s += `</div>`;
			// s = `<div class="column paddingBottomBack">`;
			// s = `<p class="difficultyLevel">` + obj.exercise + `</p>`;
			// s = `<img class="difficultyLevel1Icon" src="image/difficulty1.png" alt="Difficulty level">`
			// s += `</div>`;
			// s += `</div>`;
			// s += `</div>`;
			// //Blå
			// s = `<div class="card">`;
			// s = `<div class="front4x2blue">`;
            // s = `<p class="texttitlefront paddingTop">` + obj.exercise + `</p>`;
            // s = `<p class="underText">` + obj.exercise + `</p>`;
			// s = `<div class="row">`;
			// s = `<p class="exerciseTitle2">` + obj.exercise + `</p>`;
			// s = `<p class="exerciseText">` + obj.exercise + `</p>`;
			// s += `</div>`;
			// s = `<div class="row">`;
			// s = `<div>`;
			// s = `<p class="exercise2">` + obj.exercise + `</p>`;
			// s = `<p class="exercise2">` + obj.exercise + `</p>`;
			// s = `<p class="exercise2">` + obj.exercise + `</p>`;
			// s = `<p class="exercise2">` + obj.exercise + `</p>`;
			// s += `</div>`;
			// s = `<div>`;
			// s = `<p class="number">` + obj.exercise + `</p>`;
			// s = `<p class="number">` + obj.exercise + `</p>`;
			// s = `<p class="number">` + obj.exercise + `</p>`;
			// s = `<p class="number">` + obj.exercise + `</p>`;
			// s += `</div>`;
			// s += `</div>`;
			// s = `<p class="hashtag paddingBottom">` + obj.exercise + `</p>`;
			// s += `</div>`;
			// s = `<div class="backBlue">`;
			// s = `<p class="texttitleback paddingTopBack">` + obj.exercise + `</p>`;
			// s = `<p class="underTextBack">` + obj.exercise + `</p>`;
			// s = `<div class="row center">`;
			// s = `<div class="column timerColumn">`;
			// s = `<p class="timer">` + obj.exercise + `</p>`;
			// s = `<img class="timer" src="image/Timer.png" alt="Timer">`;
			// s += `</div>`;
			// s = `<div class="column qrColumn">`;
			// s = `<p class="qr">` + obj.exercise + `</p>`;
			// s = `<img class="qrIcon" src="image/qr.png" alt="QR kode">`;
			// s += `</div>`;
			// s += `</div>`;
			// s = `<div class="column paddingBottomBack">`;
			// s = `<p class="difficultyLevel">` + obj.exercise + `</p>`;
			// s = `<img class="difficultyLevel1Icon" src="image/difficulty1.png" alt="Difficulty level">`
			// s += `</div>`;
			// s += `</div>`;
			// s += `</div>`;
			// //Gul
			// s = `<div class="card">`;
			// s = `<div class="front4x2yellow">`;
            // s = `<p class="texttitlefront paddingTop">` + obj.exercise + `</p>`;
            // s = `<p class="underText">` + obj.exercise + `</p>`;
			// s = `<div class="row">`;
			// s = `<p class="exerciseTitle2">` + obj.exercise + `</p>`;
			// s = `<p class="exerciseText">` + obj.exercise + `</p>`;
			// s += `</div>`;
			// s = `<div class="row">`;
			// s = `<div>`;
			// s = `<p class="exercise2">` + obj.exercise + `</p>`;
			// s = `<p class="exercise2">` + obj.exercise + `</p>`;
			// s = `<p class="exercise2">` + obj.exercise + `</p>`;
			// s = `<p class="exercise2">` + obj.exercise + `</p>`;
			// s += `</div>`;
			// s = `<div>`;
			// s = `<p class="number">` + obj.exercise + `</p>`;
			// s = `<p class="number">` + obj.exercise + `</p>`;
			// s = `<p class="number">` + obj.exercise + `</p>`;
			// s = `<p class="number">` + obj.exercise + `</p>`;
			// s += `</div>`;
			// s += `</div>`;
			// s = `<p class="hashtag paddingBottom">` + obj.exercise + `</p>`;
			// s += `</div>`;
			// s = `<div class="backYellow">`;
			// s = `<p class="texttitleback paddingTopBack">` + obj.exercise + `</p>`;
			// s = `<p class="underTextBack">` + obj.exercise + `</p>`;
			// s = `<div class="row center">`;
			// s = `<div class="column timerColumn">`;
			// s = `<p class="timer">` + obj.exercise + `</p>`;
			// s = `<img class="timer" src="image/Timer.png" alt="Timer">`;
			// s += `</div>`;
			// s = `<div class="column qrColumn">`;
			// s = `<p class="qr">` + obj.exercise + `</p>`;
			// s = `<img class="qrIcon" src="image/qr.png" alt="QR kode">`;
			// s += `</div>`;
			// s += `</div>`;
			// s = `<div class="column paddingBottomBack">`;
			// s = `<p class="difficultyLevel">` + obj.exercise + `</p>`;
			// s = `<img class="difficultyLevel1Icon" src="image/difficulty1.png" alt="Difficulty level">`
			// s += `</div>`;
			// s += `</div>`;
			// s += `</div>`;
			
			
            // for (let c of obj[doo]) {       //  loop through obj (array)
            //     s += `<tr><td>${c.name}</td><td>${c.email}</td><td>${c.phone}</td></tr>`
            // }
			
        } else if (typeof obj[doo] == 'string') {
            s = obj[doo];                   // if string, place it
        }
        data = data.replaceAll(regex, s);   // actual replace
    });

    return data;
};