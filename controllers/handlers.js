'use strict';
/*
 * handlers.js
 * Requesthandlers to be called by the router mechanism
 * $ npm install cookie for user.
 */
const fs = require("fs");                                   // file system access
const httpStatus = require("http-status-codes");            // http sc
const createuser = require("../models/signup");         // models are datahandlers
const checklogin = require("../models/login");            // models are datahandlers
const profile = require("../models/profile");         // models are datahandlers
const statistics = require("../models/statistics");         // models are datahandlers
const lib = require("../controllers/libWebUtil");           // home grown utilities
const plate = require("../controllers/myTemplater");     // home grown templater
const cook = require("../controllers/sess"); 
var bcrypt = require('bcryptjs');

const getAndServe = async function (res, path, contentType) {   // asynchronous
    let args = [...arguments];                              // arguments to array
    let myargs = args.slice(3);                             // dump first three
    let obj;

    await fs.readFile(path, function(err, data) {           // awaits async read
        if (err) {
            console.log(`Not found file: ${path}`);
        } else {
            res.writeHead(httpStatus.OK, {                  // yes, write header
                "Content-Type": contentType
            });
               if(path === "views/statistics.html"){
                while( typeof (obj = myargs.shift()) !== 'undefined' ) {
                    data = plate.doStatistics(data, obj)
                }
               } else{
                while( typeof (obj = myargs.shift()) !== 'undefined' ) {
                    data = plate.doTheMagic(data, obj)
              }
               }                                            // call templater
         
            res.write(data);
            res.end();
        }
    });
};


const isLoggedIn = async function (req, res) {
    let session = cook.cookieObj(req, res);                 // create session object
    let chk = session.get('login', { signed: true });
    console.log(`isLoggedIn: ${chk}`);
    return chk == undefined ? false : true; 
};

module.exports = {
    home(req, res) {
        console.log("in home handler");
        let session = cook.cookieObj(req, res);    // create session object
        let chk = session.get('login', { signed: true });
        let path = "views/dashboard.html";
        let content = "text/html; charset=utf-8";
        getAndServe(res, path, content);
    },
    login(req, res) {
        console.log("in login handler");
        let path = "views/login.html";
        let content = "text/html; charset=utf-8";
        getAndServe(res, path, content);
    },
    other(req, res) {
        console.log("in other handler");
        let path = "views" + req.url + ".html";
        let content = "text/html; charset=utf-8";
        getAndServe(res, path, content);
    },
    signup(req,res) {
        console.log("in signup handler");
        let path = "views" + req.url + ".html";
        let content = "text/html; charset=utf-8";
        getAndServe(res, path, content, {errormsg: '', errormsg2: ''});
    },
    async profile(req,res) {
        console.log("in profile");
        if (! await isLoggedIn(req, res)) {
            req.url = "/login";                                     // repoint req
            await module.exports.login(req, res);                         // go to login page
        } else {
            let session = await cook.cookieObj(req, res);    // create session object
            let user = await session.get('login', { signed: true });
            let person = user.split(",");
            let r = await profile.getprofiledata(req, res, person);
            let path = "views" + req.url + ".html";
            let content = "text/html; charset=utf-8";
            if(person[1] === "2"){
                getAndServe(res, path, content, {usernavn: r[0][0].username, realname: r[0][0].realname, øvelse: r[0][0].exercisedone + '', color: r[0][0].themecolor});
            }else{
                getAndServe(res, path, content, {usernavn: r[0][0].username, realname: r[0][0].realname, øvelse: r[0][0].exercisedone + '', color: r[0][0].themecolor, displaynone: 'displayNone'});
            }
        }
    },
    async settings(req,res) {
        console.log("in settings");

      
        if (! await isLoggedIn(req, res)) {
            console.log("in scope check");
            req.url = "/login";                                     // repoint req
            await module.exports.login(req, res);
        }else{
        let session = await cook.cookieObj(req, res);    // create session object
        let user = await session.get('login', { signed: true });
        let person = user.split(",");
        let r = await profile.getprofiledata(req, res, person);
        let path = "views/settings.html";
        let content = "text/html; charset=utf-8";
        if(person[1] === "2"){
        getAndServe(res, path, content, {username: r[0][0].username, realname: r[0][0].realname, password: person[2] + '', email: r[0][0].email, color: r[0][0].themecolor});
        }else{
        getAndServe(res, path, content, {username: r[0][0].username, realname: r[0][0].realname, password: person[2] + '', email: r[0][0].email, color: r[0][0].themecolor, displaynone: 'displayNone'});
        }
    }
    },
    async dashboard(req,res) {
        console.log("in dashboard handler");
        let value = await isLoggedIn(req, res);
        if (!value) {
            console.log("in scope check");
            req.url = "/login";                                     // repoint req
            await module.exports.login(req, res);   
        } else {
            let session = cook.cookieObj(req, res);    // create session object
            let user = session.get('login', { signed: true });
            let person = user.split(",");
            let r = await profile.getprofiledata(req, res, person);
//            console.log(r);
            let path = "views/dashboard.html";
            let content = "text/html; charset=utf-8";
            if(person[1] === "2"){
            getAndServe(res, path, content, {displaynonebruger: 'displayNone', color: r[0][0].themecolor});
            }else{
            getAndServe(res, path, content, {displaynone: 'displayNone', color: r[0][0].themecolor});
            }
        }
    },

    async statistics(req,res) {
        console.log("in statistics");
       
        if (! await isLoggedIn(req, res)) {
            console.log("in scope check");
            req.url = "/login";                                     // repoint req
            await module.exports.login(req, res); 
        }else{
        let session = await cook.cookieObj(req, res);    // create session object
        let user = await session.get('login', { signed: true });
        let person = user.split(",");
        let r = await statistics.getprofiledata(req, res, person);
        // console.log(r);
        let tabler = await statistics.getstatistics(req, res, r);
        // console.log(tabler);
        let path = "views/statistics.html";
        let content = "text/html; charset=utf-8";
        getAndServe(res, path, content);
        if(person[1] === "2"){
        getAndServe(res, path, content, {statistictable: tabler, color: r[0][0].themecolor});
        }else{
            req.url = "/dashboard";                                     // repoint req
            await module.exports.dashboard(req, res); 
        }
    }
    },
    js(req, res) {
        let path = "public/javascripts" + req.url;
        let content = "application/javascript; charset=utf-8";
        getAndServe(res, path, content);
    },
    css(req, res) {
        let path = "public/stylesheets" + req.url;
        let content = "text/css; charset=utf-8";
        getAndServe(res, path, content);
    },
    png(req, res) {
        let path = "public/images" + req.url;
        let content = "image/png";
        getAndServe(res, path, content);
    },
    jpg(req, res) {
        let path = "public/images" + req.url;
        let content = "image/jpeg";
        getAndServe(res, path, content);
    },
    svg(req, res) {
        let path = "public" + req.url;
        let content = "image/svg+xml";
        getAndServe(res, path, content);
    },
    ico(req, res) {
        let path ="public" + req.url;
        let content = "image/x-icon";
        getAndServe(res, path, content);
    },

    notfound(req, res) {
        console.log(`Handler 'notfound' was called for route ${req.url}`);
        res.end();
    },

    async receiveSignup(req, res, data) {
        let obj = lib.makeWebArrays(req, data);         // home made GET and POST objects
        let r = await createuser.signup(req, res, obj);
        // console.log(`debug: ${typeof r}, ${r}`);
        if(obj.POST.password === obj.POST.password2){
        if (r === false) {
            let path = req.url;
            path = "views" + path + ".html";            
            let content = "text/html; charset=utf-8";
            getAndServe(res, path, content, {errormsg: 'email eksistere allerede', errormsg2: ''});
        } else {
            res.writeHead(httpStatus.MOVED_PERMANENTLY, {"Location": "http://localhost:3000/"});
            res.end();
        }
        }else{
            let path = req.url;
            path = "views" + path + ".html";            
            let content = "text/html; charset=utf-8";
            getAndServe(res, path, content, {errormsg2: 'password stemmer ikke overens', errormsg: ''});
        }

    },
    async checkLogin(req, res, data) {
        let session = await cook.cookieObj(req, res);      
        let obj = lib.makeWebArrays(req, data);  // home made GET and POST objects
        let r = await checklogin.checkUsers(req, res, obj);
        // console.log(JSON.stringify(r));
        if (r.length >= 1 && await bcrypt.compare(obj.POST.password, ''+r[0][0].password)){
            let personAndRole = '' + obj.POST.username + ',' + r[0][0].role + '' + ',' + obj.POST.password; 
            await session.set('login', { signed: true, expires: Date.now() })                  // unset login cookie
            let setdate = new Date();
            await setdate.setMinutes(setdate.getMinutes() + (24 * 60));
            let value = await session.set('login', personAndRole, { signed: true, expires: setdate});       // set login cookie whit key: variable and expiredate of one month.
            console.log("logged in as: " +obj.POST.username );
            console.log("logged in as: " +obj.POST.username + ' sesvar: ' + value);
            req.url = "/dashboard";                                     // repoint req
            await module.exports.dashboard(req, res);                         // go to login page
                // module.exports.home(req, res);                      // go to home page
        } else {
            req.url = "/logtheuserout";                                     // repoint req
            await module.exports.logout(req, res);                         // go to login page
        }

    },
    async logout (req, res) {
         console.log("in logout");
        let session = await cook.cookieObj(req, res);  
        await session.set('login', { signed: true, expires: Date.now() })                  // unset login cookie
        req.url = "/login";                                     // repoint req
        await module.exports.login(req, res);                        // go to login page
    },
    async deleteUser (req, res) {
        console.log("in deleteUser");

        if (! await isLoggedIn(req, res)) {
            req.url = "/login";                                     // repoint req
            await module.exports.login(req, res);
        }else{
        let session = await cook.cookieObj(req, res);        
        let user = await session.get('login', { signed: true });
        let person = user.split(",");
        let r = await profile.deleteUser(req, res, person);                // unset login cookie
        if(r === true){
            let personAndRole = 'undefined';               // create session object
            await session.set('login', personAndRole, { signed: true })     
            console.log("deleted");
            req.url = "/logtheuserout";                                     // repoint req
            await module.exports.logout(req, res);
        }else{
            console.log("Not deleted");
            req.url = "/settings";                                     // repoint req
            await module.exports.settings(req, res);
        }
    }
    },
    async updateUser (req, res, data) {
        console.log("in update user");
        let obj = lib.makeWebArrays(req, data);    
        if (! await isLoggedIn(req, res)) {
            req.url = "/login";                                     // repoint req
            await module.exports.login(req, res);                         // go to login page
        }else{
            console.log(obj);
            let session = await cook.cookieObj(req, res);        
            let user = await session.get('login', { signed: true });
            let person = user.split(",");
            let r = await profile.updateUser(req, res, obj, person);
            if(r === true){
                console.log("in scope");
                let personAndRole = '' + obj.POST.username + ',' + person[1] + '' + ',' + obj.POST.password + ''; 
                await session.set('login', personAndRole, { signed: true, maxAge: 2678400});
                req.url = "/profile";                                     // repoint req
                await module.exports.profile(req, res);
            }    
        }                                                  
    }

}