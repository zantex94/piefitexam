drop database if exists piefit;
create database piefit;
use piefit

create table user (
    email varchar(150) not null,
    realname varchar(100) not null,
    username varchar(100) not null,
    password varchar(100) not null,
    role int(10) not null,
    exercisedone bigint not null,
    themecolor varchar(100) not null,
    primary key(email, username)

);
create table collection (
    id bigint not null auto_increment primary key,
    name varchar(150) not null,
    description varchar(500) not null,
    amount bigint not null
    
);
create table usercollection (
    id bigint not null,
    email varchar(150) not null,
    primary key(id, email),
    foreign key(id) references collection(id) on delete cascade,
    foreign key(email) references user(email) on delete cascade
);
create table category (
    id bigint not null auto_increment primary key,
    name varchar(150) not null,
    color varchar(7) not null
);

create table card (
    id bigint unsigned auto_increment not null,
    texttitlefront varchar(150) not null,
    texttitleback varchar(150) not null,
    cardlevel int(5) not null,
    exerciseform varchar(150) not null,
    hashtext varchar(150) not null,
    videolink varchar(500) not null,
    catid bigint not null,
    primary key(id,catid),
    foreign key(catid) references category(id) on delete cascade

);
create table cardconnection (
    id bigint not null,
    cardid bigint unsigned not null,
    primary key(id,cardid),
    foreign key(cardid) references card(id) on delete cascade,
    foreign key(id) references collection(id) on delete cascade
);

create table repetition (
    id bigint unsigned auto_increment not null,
    rep varchar(50) not null,
    repamount bigint not null, 
    cardid bigint unsigned not null,
    primary key(id, cardid),
    foreign key(cardid) references card(id) on delete cascade
);
create table rounds (
    id bigint unsigned auto_increment not null,
    roundcount varchar(50) not null,
    cardid bigint unsigned not null,
    primary key(id, cardid),
    foreign key(cardid) references card(id) on delete cascade
);
create table exerciseback (
    id bigint unsigned not null primary key,
    level varchar(50) not null,
    foreign key(id) references card(id) on delete cascade
);
create table maxload (
    id bigint unsigned auto_increment not null,
    exercisebackid bigint unsigned not null,
    percenteload double not null,
    primary key(id, exercisebackid),
    foreign key(exercisebackid) references exerciseback(id) on delete cascade
);
create table exerciseround (
    id bigint unsigned auto_increment not null,
    exercisebackid bigint unsigned not null,
    rounds varchar(50) not null,
    primary key(id, exercisebackid),
    foreign key(exercisebackid) references exerciseback(id) on delete cascade
);
create table exercisebacktime (
    id bigint unsigned auto_increment not null,
    exercisebackid bigint unsigned not null,
    roundtime time not null,
    primary key(id, exercisebackid),
    foreign key(exercisebackid) references exerciseback(id) on delete cascade
);
create table singletime (
    id bigint unsigned not null primary key,
    exercisetime time not null,
    foreign key(id) references card(id) on delete cascade
);
create table cardrealease (
    id bigint unsigned not null primary key,
    releasedate datetime not null,
    foreign key(id) references card(id) on delete cascade
);
create table equitment (
    id bigint unsigned auto_increment not null,
    cardid bigint unsigned not null,
    equitmentname varchar(50) not null,
    picture blob not null,
    primary key(id, cardid),
    foreign key(cardid) references card(id) on delete cascade
);

create table farvourites (
    id bigint unsigned not null,
    email varchar(150) not null,
    primary key(id, email),
    foreign key(id) references card(id) on delete cascade,
    foreign key(email) references user(email) on delete cascade
);
create table statistics (
    id bigint unsigned not null,
    email varchar(150) not null,
    description varchar(150) not null,
    datedone datetime not null,
    primary key(id, email),
    foreign key(email) references user(email) on delete cascade,
    foreign key(id) references card(id) on delete cascade
);

INSERT INTO `user` (`email`, `realname`, `username`, `password`, `role`, `exercisedone`, `themecolor`) VALUES
('martin@gmail.com', 'Martin Kidmose', 'martin', '$2a$10$WsoB1H/HUF2W0YzRtCzXF.Q90wvNAkURJlEnVIDC8A2OpIXMgNSdS', 1, 0, '#C6D9DB'),
('reneseer@gmail.com', 'René Seebach', 'zantex94', '$2a$10$QbiWAAYpsGrdTgXmp28/pOUzvkc0hP8xvbiDz95/sOF2PowO3KbZy', 2, 0, '#C6D9DB'),
('tobias@gmail.com', 'Tobias ', 'tobias', '$2a$10$U1dku5e7huzaYrRey1Ru0.rHW/KVZJMyd6cy6dUC6lUva5.gAhXY2', 2, 0, '#C6D9DB');

INSERT INTO `collection` (`id`, `name`, `description`, `amount`) VALUES
(1, 'HOME EDITION', 'Træninger hvor som helst når som helst - helt uden udstyr', 40),
(2, 'HIIT EDITION', 'Træninger med en træningselastik - høj puls og høj forbrænding', 40),
(3, 'POWER EDITION', 'Træninger med udstyr - bliv presset på intensitet, byg muskler og styrke', 40);

INSERT INTO `category` (`id`, `name`, `color`) VALUES
(1, 'CORE', '#C6D9DB'),
(2, 'LOWERBODY', '#DAE889'),
(3, 'UPPERBODY', '#F8B2BA'),
(4, 'FULLBODY', '#FFE67B');

INSERT INTO `card` (`id`, `texttitlefront`, `texttitleback`, `cardlevel`, `exerciseform`, `hashtext`, `videolink`, `catid`) VALUES
(1, 'REPS/ROUNDS', 'REFERENCES', 1, 'BEGINNER-WARMUP-FINISHER', '#PiefitSanJose', 'https://piefitcards.dk/WashingtonDC/', 1),(2, 'ARMRAP 15', 'BENCHMARK', 3, 'BEGINNER-WARMUP-FINISHER', '#PiefitMalé', 'https://piefitcards.dk/WashingtonDC/', 1) ;

INSERT INTO `rounds` (`id`, `roundcount`, `cardid`) VALUES
(1, '4 ROUNDS', 1),(2, '4 ROUNDS', 1);

INSERT INTO `singletime` (`id`, `exercisetime`) VALUES ('1', '00:12:38'),('2', '00:32:38');

INSERT INTO `statistics` (`id`, `email`, `description`, `datedone`) VALUES
(1, 'reneseer@gmail.com', 'Gik godt!', '2021-12-12 16:36:02'),
(1, 'tobias@gmail.com', 'Gik super godt for mig.', '2021-12-12 12:36:03'),
(2, 'reneseer@gmail.com', 'Gik mere end godt!', '2021-11-12 17:33:02');

INSERT INTO `repetition` (`rep`, `repamount`, `cardid`) VALUES
('BUTT UP', 15, 1), ('KNEE 2 ELBOW', 30, 1),
('NARROW PUSH UP*', 10, 1),
('WALK OUT', 10, 1),
('KNEE TO BLOW', 30, 2),
('WALK OUT', 10, 2),
('NARROW PUSH UP*', 10, 2),
('BUTT UP', 10, 2);






