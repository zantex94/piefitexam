'use strict';
/*
 * models
 * handlers for data manipulation
 */
const maria = require("mysql2/promise");                               // file system access
const dbp = require('./dbParams.js');
var bcrypt = require('bcryptjs');

module.exports = {
    async getprofiledata(req, res, user) {
            const dbh = await maria.createConnection(dbp);
            let sql =`select * from user where username = '${user[0]}'`
            let row = await dbh.execute(sql);
            return row;
    },
    async deleteUser(req, res, user) {
            try{

       
            const dbh = await maria.createConnection(dbp);
            let sql =`delete from user where username='${user[0]}';`
            await dbh.execute(sql);
            return true;
         }catch(e){
                return false;
                }
    },

    async updateUser(req, res, obj, user) {
            try{
            const dbh = await maria.createConnection(dbp);
            let hashed = await bcrypt.hash(obj.POST.password, 10);
            let sql =`update user set username="${obj.POST.username}", realname="${obj.POST.realname}", password="${hashed}", email="${obj.POST.email}", themecolor="${obj.POST.color}" where username='${user[0]}';`
            await dbh.execute(sql);
            return true;
            }catch(e){
            return false;
            }
            }
        

            
    }
