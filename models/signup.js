'use strict';
/*
 * models
 * handlers for data manipulation
 */
const maria = require("mysql2/promise");                               // file system access
const dbp = require('./dbParams.js');
var bcrypt = require('bcryptjs');

module.exports = {
    async signup(req, res, obj) {
        try {
            const dbh = await maria.createConnection(dbp);
            const role = 2;
            const ed = 0;
            const c = '#C6D9DB';
            let hashed = await bcrypt.hash(obj.POST.password, 10);
            let sql = `insert into user values('${obj.POST.email}', '${obj.POST.realname}', '${obj.POST.username}','${hashed}','${role}','${ed}','${c}')`
            await dbh.execute(sql);
            return true;
        } catch (e) {
            return false;
        }
    }
}
