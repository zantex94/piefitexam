"use strict";
let colorValue = document.getElementById("color").value;
const body = document.getElementsByTagName("body")[0];
const themeSelected = document.getElementById("theme-select");
let currentTheme = "blue";

window.onload = function () {
  themeSelected.addEventListener("change", function (e) {
    let newTheme = e.currentTarget.value;
    themeGo(currentTheme, newTheme);
  });

  const themeGo = function (oldTheme, newTheme) {
    body.classList.remove(oldTheme);
    body.classList.add(newTheme);
    currentTheme, colorValue = newTheme;
    console.log(colorValue);
  };
};
