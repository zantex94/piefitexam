'use strict';
// import {makeMenu, setTitles, setFooter} from './nQm.js';


function popup(){

    const container = document.getElementById("popup");
    const redBtn = document.getElementsByClassName("popup-red");
    const popBtn = document.getElementById("popBtn").addEventListener("click", function(){container.style.display = "block";});
    
    const closePopup = function() {
        container.style.display = "none";
    }
    redBtn[0].addEventListener("click", closePopup);

}

function toggleColor(){
    const div1 = document.getElementById("#C6D9DB");
    const div2 = document.getElementById("#dae889");
    const div3 = document.getElementById("#ffe67b");
    const div4 = document.getElementById("#F8B2BA");

    div1.addEventListener("click", function(){
        document.getElementById("color").value = "#C6D9DB";
        this.style.outline = "2px solid black";
        div2.style.outline = "none";
        div3.style.outline = "none";
        div4.style.outline = "none";


    });

    div2.addEventListener("click", function(){
        document.getElementById("color").value = "#dae889";
        this.style.outline = "2px solid black";
        div1.style.outline = "none";
        div3.style.outline = "none";
        div4.style.outline = "none";


    });

    div3.addEventListener("click", function(){
        document.getElementById("color").value = "#ffe67b";
        this.style.outline = "2px solid black";
        div1.style.outline = "none";
        div2.style.outline = "none";
        div4.style.outline = "none";
    });

    div4.addEventListener("click", function(){
        document.getElementById("color").value = "#F8B2BA";
        this.style.outline = "2px solid black";
        div1.style.outline = "none";
        div2.style.outline = "none";
        div3.style.outline = "none";
    });
 
    

}
function toggleStart(){
    // console.log("in toggleStart")
    const input = document.getElementById("color").value;
    const div1 = document.getElementById("#C6D9DB");
    const div2 = document.getElementById("#dae889");
    const div3 = document.getElementById("#ffe67b");
    const div4 = document.getElementById("#F8B2BA");
    console.log("in toggleStart " + input);

    switch(input){
        case "#C6D9DB":
            div1.style.outline = "2px solid black";
            break;
        case "#dae889":
            div2.style.outline = "2px solid black";
            break;
        case "#ffe67b":
            div3.style.outline = "2px solid black";
            break;
        case "#F8B2BA":
            div4.style.outline = "2px solid black";
            break;

    }
}

const doSomething = function () {
    toggleStart();
    toggleColor();
    // popup();
    // makeMenu('menu');
    // setTitles('Settings');
    // setFooter('piefit', 2021);
}
window.addEventListener('load', doSomething);
