'use strict';
import {makeMenu, setTitles, setFooter} from './nQm.js';

const doSomething = function () {
    makeMenu('menu');
    setTitles('Farvorite');
    setFooter('piefit', 2021);
}
window.addEventListener('load', doSomething);
