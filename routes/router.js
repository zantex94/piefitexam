"use strict";
/*
 * check if routed handler function exists
 * if yes call it, else complain
 */
const handlers = require("../controllers/handlers");  // handlers module
const requestHandlers = {                             // application urls here
    GET: {
        "/": handlers.login,
        "/login": handlers.login,
        "/settings": handlers.settings,
        "/profile": handlers.profile,
        "/dashboard": handlers.dashboard,
        "/favorites": handlers.home,
        "/createuser": handlers.signup,
        "/logout": handlers.logout,
        "/logtheuserout": handlers.logout,
        "/deletetheuser": handlers.deleteUser,
        "/statistics": handlers.statistics,
        "/notfound": handlers.notfound,
        "js": handlers.js,
        "css": handlers.css,
        "png": handlers.png,
        "jpg": handlers.jpg,
        "svg": handlers.svg,
        "ico": handlers.ico
    },
    POST: {
        "/createuser": handlers.receiveSignup,
        "/login": handlers.checkLogin,
        "/settings": handlers.updateUser

    }
}

module.exports = {
    route(req, res, bodydata) {
        let urls = req.url.split("?");              // separate query string from url
        req.url = urls[0];                          // clean url
        let arr = req.url.split(".");               // filename with suffix
        let ext = arr[arr.length - 1];              // get suffix
        if (typeof requestHandlers[req.method][req.url] === 'function') {  // look for route
            requestHandlers[req.method][req.url](req, res, bodydata);      // if found use it
        } else if (typeof requestHandlers[req.method][ext] === "function") {
            requestHandlers[req.method][ext](req, res);
        } else {
            requestHandlers.GET["/notfound"](req, res);        // use notfound
        }
    }
}